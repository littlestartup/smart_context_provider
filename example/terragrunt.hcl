locals {
  region = "eu-central-1"
  account_hash = substr(md5(get_aws_account_id()), 0, 5)
  region_hash  = substr(md5(local.region), 0, 5)
  service_name = "xyz"
  stage = "dev"
}

remote_state {
  backend = "s3"
  generate = {
    path      = "00_generated_00_backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = join("-", [ local.account_hash, local.region_hash, "tf", "states"])
    key    = join("/", [ local.stage, local.service_name, "terraform.tfstate"])
    region = local.region
    encrypt = true
  }
}

generate "provider" {
  path = "00_generated_01_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "aws" {
  region = "${local.region}"
}
EOF
}

inputs = {
    service_prefix = join("-", [ local.account_hash, local.region_hash, local.service_name])
    api_gateway_ip_whitelist = [ "144.76.139.200/32", "5.100.63.118/32"]
    gitlab_api = get_env("gitlab_api", "https://gitlab.com/api/v4")
    gitlab_api_token = get_env("gitlab_api_token", "")
    assume_role_whitelist = [ "*" ]
    # example_assume_role_names = [ "iac_ro", "iac_rw", "kubernetes_ro", "no_access", "s3_upload" ]
    service_version = get_env("VERSION", "1.0")
    base_dir = "./sample-config"
}

terraform {
  source = "../terraform"
}
