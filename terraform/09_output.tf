output "api_gateway_url" {
  value = "${aws_api_gateway_stage.stage.invoke_url}/context?token="
}

output "bucket" {
  value = aws_s3_bucket.bucket.id
}
