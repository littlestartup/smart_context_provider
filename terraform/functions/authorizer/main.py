import os
import json
import jwt
import requests

from aws_lambda_powertools import Logger, Tracer
from aws_lambda_powertools.logging import correlation_paths
tracer = Tracer()
logger = Logger()

JWKS_URL = os.environ["JWKS_URL"]
JWK_ISSUER = os.environ["JWK_ISSUER"]


@tracer.capture_lambda_handler
@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
def handler(event, context):
    return authorize(event)


@tracer.capture_method
def get_token(event):
    token = event.get('queryStringParameters').get('token', None)
    if token.startswith("Bearer "):
        token = token[7:]
    print("token", token)
    return token


@tracer.capture_method
def authorize(event):
    method_arn = event.get('methodArn', "NOT_PROVIDED")
    token = get_token(event)

    authorization_context = {}

    try:
        jwks = requests.get(JWKS_URL).json()
        kid = jwt.get_unverified_header(token)['kid']
        try:
            for key in jwks['keys']:
                if key['kid'] == kid:
                    authorization_context = jwt.decode(
                        token,
                        jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(key)),
                        algorithms=[key['alg']],
                        issuer=JWK_ISSUER
                    )
        except Exception as jwt_exception:
            print(jwt_exception)
    except Exception as jwks_exception:
        print(jwks_exception)

    return format_output(method_arn, authorization_context)


@tracer.capture_method
def format_output(method_arn, authorization_context):
    user = authorization_context.get('user_email', 'NOBODY')
    tracer.put_annotation(key="user", value=user)
    output = {
        "principalId": user,
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": "Allow" if 'user_email' in authorization_context else "Deny",
                    "Resource": method_arn
                }
            ]
        },
        "context": authorization_context
    }
    return output
