locals {
  api_gateway_stage_name = "gitlab"
  api_gateway_name       = join("-", [var.service_prefix, "api"])
}

resource "aws_api_gateway_rest_api" "api" {
  name = local.api_gateway_name

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  body = templatefile("${path.module}/03_api.yml", {
    title           = var.service_prefix
    version         = var.service_version
    api_uri         = aws_lambda_function.api.invoke_arn
    authorizer_uri  = aws_lambda_function.authorizer.invoke_arn
    authorizer_role = aws_iam_role.authorizer_invocation_role.arn
  })

  tags = merge(
    var.tags,
    {}
  )
}

resource "aws_cloudwatch_log_group" "api" {
  name              = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.api.id}/${local.api_gateway_stage_name}"
  retention_in_days = 3

  tags = merge(
    var.tags,
    {
      "Name" : join(":", [local.api_gateway_name, local.api_gateway_stage_name])
    }
  )
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_api_gateway_rest_api_policy" "api" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  policy      = data.aws_iam_policy_document.api.json
}

data "aws_iam_policy_document" "api" {
  statement {

    sid = "allow-all"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = [
      "execute-api:Invoke"
    ]

    resources = [
      "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
    ]
  }

  statement {

    sid = "allow-just-whitelisted-ips"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = [
      "execute-api:Invoke"
    ]

    resources = [
      "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
    ]

    condition {
      test     = "NotIpAddress"
      variable = "aws:SourceIp"

      values = var.api_gateway_ip_whitelist
    }

    effect = "Deny"
  }
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_api_gateway_stage" "stage" {
  deployment_id        = aws_api_gateway_deployment.deployment.id
  rest_api_id          = aws_api_gateway_rest_api.api.id
  stage_name           = local.api_gateway_stage_name
  xray_tracing_enabled = true
 
  tags = merge(
    var.tags,
    {
      "Name" : join("-", [var.service_prefix, local.api_gateway_stage_name])
    }
  )
}

resource "aws_api_gateway_method_settings" "settings" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
  method_path = "*/*"

  settings {
    metrics_enabled        = true
    throttling_burst_limit = 10
    throttling_rate_limit  = 20
  }
}

resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.api.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_ssm_parameter" "api_gateway_endpoint" {
  name  = local.api_gateway_name
  type  = "String"
  value = "${aws_api_gateway_stage.stage.invoke_url}/context?token="
}

resource "aws_api_gateway_base_path_mapping" "mapping" {
  count       = length(var.api_gateway_domain) == 0 ? 0 : 1 
  api_id      = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
  domain_name = var.api_gateway_domain
  base_path   = var.api_mapping_path
  depends_on = [
    aws_api_gateway_deployment.deployment,
    aws_api_gateway_stage.stage
  ]
}

