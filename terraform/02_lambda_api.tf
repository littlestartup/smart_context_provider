locals {
  api_function_name = join("-", [var.service_prefix, "api"])
}

data "archive_file" "api" {
  type        = "zip"
  source_dir  = "${path.module}/functions/api"
  output_path = "${path.module}/.tmp/api.zip"
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "api" {
  name               = local.api_function_name
  assume_role_policy = data.aws_iam_policy_document.assume_lambda.json
}

resource "aws_iam_role_policy_attachment" "api_basic_execution" {
  role       = aws_iam_role.api.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/${local.basic_execution_role_name}"
}

resource "aws_iam_role_policy_attachment" "api_xray" {
  role       = aws_iam_role.api.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

resource "aws_iam_role_policy" "api" {
  name   = "inline"
  role   = aws_iam_role.api.id
  policy = data.aws_iam_policy_document.api_function.json
}

data "aws_iam_policy_document" "api_function" {
  statement {
    sid = "AllowReadS3"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetObjectTagging"
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.bucket.id}",
      "arn:aws:s3:::${aws_s3_bucket.bucket.id}/*"
    ]
  }

  statement {
    sid = "AllowAssumeToRoleWhitelist"

    actions = [
      "sts:AssumeRole",
      "sts:TagSession"
    ]

    resources = var.assume_role_whitelist
  }

  statement {
    sid = "GetSSMParameters"

    actions = [
      "ssm:GetParameter"
    ]

    resources = [
      "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/${local.gitlab_api_token}"
    ]
  }
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_cloudwatch_log_group" "api_function" {
  name              = "/aws/lambda/${local.api_function_name}"
  retention_in_days = 3

  tags = merge(var.tags,
    {
      "Name" : local.api_function_name
    }
  )
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_lambda_function" "api" {
  filename         = data.archive_file.api.output_path
  source_code_hash = data.archive_file.api.output_base64sha256

  function_name = local.api_function_name
  role          = aws_iam_role.api.arn

  layers = [local.lambda_layer_arn]

  handler = "main.handler"
  runtime = "python3.9"

  timeout     = 10
  memory_size = 256

  reserved_concurrent_executions = 10

  tags = merge(
    var.tags,
    {
      "Name" : local.api_function_name
    }
  )

  vpc_config {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }

  environment {
    variables = {
      "S3_BUCKET_NAME" : aws_s3_bucket.bucket.id,
      "GITLAB_API" : var.gitlab_api
      "SSM_GITLAB_API_TOKEN" : local.gitlab_api_token
      "LOG_LEVEL": "INFO"
      "POWERTOOLS_SERVICE_NAME": var.service_prefix
    }
  }

  tracing_config {
    mode = "Active"
  }
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_lambda_permission" "api_apigateway" {
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  function_name = local.api_function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
}
