# SCP usage as a terraform module

The best way to integrate SCP in your environment is by use it as a terraform module\
You can wrap you own Infra arround by defining an own vpc, endpoints, custom domain name, route53\
Most of the things are not required - important is that the gitlab is reachable from your lambda functions

```hcl
module "smart_context_provider" {
  source = "git@gitlab.com:littlestartup/smart_context_provider.git//terraform?ref=1.0.1"

  service_prefix  = var.service_prefix
  service_version = var.service_version

  # optional subnet and security group - if not provided aws default scope
  # subnet_ids         = data.aws_subnet_ids.subnet.ids
  # security_group_ids = [aws_security_group.smart_context_provider.id]

  # optional ip whitelist - if not provided accepts all
  api_gateway_ip_whitelist = [
    "192.168.0.0/24",
    "1.2.3.3/32"
  ]

  # gitlab config - if not provided gitlab is disabled and SCP is not fully working
  gitlab_api        = "https://gitlab.com/api/v4"
  gitlab_api_token  = ""
  gitlab_jwks       = "https://gitlab.com/-/jwks"
  gitlab_jwt_issuer = "gitlab.com"

  # roles which the context lambda can assume
  assume_role_whitelist = [
    "arn:aws:iam::123xxxxxxxxx:role/*", # account_1
    "arn:aws:iam::234xxxxxxxxx:role/*" # account_2
  ]

  # config base_dir - which directory is used to sync the config
  base_dir = "${path.module}/config"

  # api_gateway_domain is not required - if you want to provide a cutom domain name
  # api_gateway_domain = aws_api_gateway_domain_name.domain.domain_name
  # depends_on = [
  #   aws_api_gateway_domain_name.domain
  # ]

  tags = merge(
    local.default_tags,
    {
      "Name" : join("-", [local.service_prefix, "smart_context_provider"])
    }
  )
}
```

## How to integrate into your VPC

As a small demo you can see how to lookup an existing vpc and subnet.\
After that you have to create a security group and use these informations in the example above

```hcl
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["<VPC-NAME>"]
  }
}

data "aws_subnet_ids" "subnet" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["<SUBNET-NAME>"]
  }
}

resource "aws_security_group" "smart_context_provider" {
  name   = join("-", [local.service_prefix, "smart_context_provider"])
  vpc_id = data.aws_vpc.vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.default_tags,
    {
      "Name" : join("-", [local.service_prefix, "smart_context_provider"])
    }
  )
}
```

## How to us a custom domain name for the api-gateway

To get rid of the api-gateway specific domain and use your own domain name for scp you can create
a `custom_domain_name` in the api gateway.\
By handover the domain name into the scp module the api is mapped into your `custom_domain_name`

```hcl
variable "dns_base_domain" {
  type = string
}

data "aws_route53_zone" "zone" {
  name = var.dns_base_domain
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "scp.${var.dns_base_domain}"
  validation_method = "DNS"

  tags = merge(
    local.default_tags,
    {
      "Name" : join("-", [local.service_prefix, "smart_context_provider"])
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "verification" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.zone.zone_id
}

resource "aws_api_gateway_domain_name" "domain" {
  domain_name              = "scp.${var.dns_base_domain}"
  regional_certificate_arn = aws_acm_certificate.cert.arn
  security_policy          = "TLS_1_2"

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  tags = merge(
    local.default_tags,
    {}
  )

  depends_on = [
    aws_acm_certificate.cert,
    aws_route53_record.verification
  ]
}

resource "aws_route53_record" "scp" {
  name    = aws_api_gateway_domain_name.domain.domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.zone.id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.domain.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.domain.regional_zone_id
  }
}
```
